<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_rektor extends CI_Model {
	
	
	
	
	function get($where="") {
		
		if($where){
			
			$this->db->where($where);
		
		}
		
		$query = $this->db->get('rektor');
		
		
		return $query;
		$query->free_result();
		
	}
	
	
		
	function update($where,$data) {
		
		$this->db->set($data);
		$this->db->where($where);
		$this->db->update('rektor');
		
	}
	
	function getdata($where) {
		
		$this->load->library('datatables');
       		$this->datatables->select('id_rektor,nip,nama_rektor,waktu_akhir_jabatan');
		$this->datatables->add_column("aksi"
		,'
	
		<a class="waves-effect waves-light" onclick="getforupdate($1)"><i class="blue-text fas fa-edit fa-2x"></i></a>
		<a class="waves-effect waves-light" onclick="detail($1)"><i class="yellow-text fas fa-info fa-2x"></i></a>
		<a class="waves-effect waves-light" onclick="deletedata($1)"><i class="red-text fas fa-trash fa-2x"></i></a>'
		, 'id_rektor,nip,nama_rektor,waktu_akhir_jabatan');
		
		if($where!=""){
				
			$this->datatables->where($where);
			
		}
        $this->datatables->from('rektor');
        $query=$this->datatables->generate();
		
		return $query;
		$query->free_result();
		
	}
	
	function insert($data) {
		
		$this->db->insert('rektor',$data);
		
		return;
		
	}
		
	function delete($where) {
		
		$this->db->where($where);
		$this->db->delete('rektor');
		
		return;
		
	}
	
	
	
}