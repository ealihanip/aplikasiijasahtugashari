<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_prodi extends CI_Model {
	
	
	
	
	function get($where="") {
		
		if($where){
			
			$this->db->where($where);
		
		}
		
		$query = $this->db->get('prodi');
		
		
		return $query;
		$query->free_result();
		
	}
	
	
		
	function update($where,$data) {
		
		$this->db->set($data);
		$this->db->where($where);
		$this->db->update('prodi');
		
	}
	
	function getdata($where) {
		
		$this->load->library('datatables');
       		$this->datatables->select('id_prodi,nama_prodi,gelar');
		$this->datatables->add_column("aksi"
		,'
	
		<a class="waves-effect waves-light" onclick="getforupdate($1)"><i class="blue-text fas fa-edit fa-2x"></i></a>
		<a class="waves-effect waves-light" onclick="detail($1)"><i class="yellow-text fas fa-info fa-2x"></i></a>
		<a class="waves-effect waves-light" onclick="deletedata($1)"><i class="red-text fas fa-trash fa-2x"></i></a>'
		, 'id_prodi,nama_prodi,gelar');
		
		if($where!=""){
				
			$this->datatables->where($where);
			
		}
        $this->datatables->from('prodi');
        $query=$this->datatables->generate();
		
		return $query;
		$query->free_result();
		
	}
	
	function insert($data) {
		
		$this->db->insert('prodi',$data);
		
		return;
		
	}
		
	function delete($where) {
		
		$this->db->where($where);
		$this->db->delete('prodi');
		
		return;
		
	}
	
	
	
}