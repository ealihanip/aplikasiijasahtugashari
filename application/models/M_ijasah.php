<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_ijasah extends CI_Model {
	
	
	
	
	function get($where="") {
		
		if($where){
			
			$this->db->where($where);
		
		}
		
		$query = $this->db->get('ijasah');
		
		
		return $query;
		$query->free_result();
		
	}
	
	
		
	function update($where,$data) {
		
		$this->db->set($data);
		$this->db->where($where);
		$this->db->update('ijasah');
		
	}
	
	function getdata($where) {
		
		$this->load->library('datatables');
       	$this->datatables->select('id_cetak,seri_ijasah,no_ijasah,nim,nama,prodi');
		$this->datatables->add_column("aksi"
		,'
	
		<a class="waves-effect waves-light" onclick="cetak($1)"><i class="blue-text fas fa-print fa-2x"></i></a>
		'
		, 'id_cetak,seri_ijasah');
		
		if($where!=""){
				
			$this->datatables->where($where);
			
		}
        $this->datatables->from('ijasah');
        $query=$this->datatables->generate();
		
		return $query;
		$query->free_result();
		
	}
	
	function insert($data) {
		
		$this->db->insert('ijasah',$data);
		
		return;
		
	}
		
	function delete($where) {
		
		$this->db->where($where);
		$this->db->delete('ijasah');
		
		return;
		
	}


	function getdekan(){

		
		$this->db->order_by('waktu_akhir_jabatan', 'asc');
		$query = $this->db->get('dekan');
		
		
		return $query;
		$query->free_result();


	}

	function getrektor(){

		
		$this->db->order_by('waktu_akhir_jabatan', 'asc');
		$query = $this->db->get('rektor');
		
		
		return $query;
		$query->free_result();


	}

	function getcetak($where="") {
		
		if($where){
			
			$this->db->where($where);
		
		}
		$this->db->select('
		
		
		
			ijasah.*,prodi.*,rektor.*,
			dekan1.nama_dekan as nama_dekan_lulus,
			dekan1.nip as nip_dekan_lulus,

			dekan2.nama_dekan as nama_dekan_cetak,
			dekan2.nip as nip_dekan_cetak,
		
		
		
		');
		$this->db->join('prodi', 'ijasah.prodi = prodi.id_prodi');
		$this->db->join('rektor', 'ijasah.rektor = rektor.id_rektor');
		$this->db->join('dekan as dekan1', 'ijasah.dekan_lulus = dekan1.id_dekan');
		$this->db->join('dekan as dekan2', 'ijasah.dekan_cetak = dekan2.id_dekan');
		
		$query = $this->db->get('ijasah');
		
		
		return $query;
		$query->free_result();
		
	}

	function getijasah($where="") {
		
		if($where){
			
			$this->db->where($where);
		
		}
		$query = $this->db->get('ijasah');
		
		
		return $query;
		$query->free_result();
		
	}

	function getnocetak() {
		
		
		$query = $this->db->get('ijasah');
		
		
		return $query;
		$query->free_result();
		
	}
	
	
	
}