<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rektor extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('m_rektor');
	}
	
	
	public function get(){
		$id = $this->input->post('id');			
		$where = array(

			'id_rektor' => $id
			
		);
		
		
		
		$data['data']=$this->m_rektor->get($where)->result_array();
		
		
		echo json_encode($data);
	}
	
	
	
	public function getdata(){
		
		
       if($this->uri->segment('3')){
			$where = array(

				'id_rektor' => $this->uri->segment('3')
					
			);
	   }else{
		   
		  $where = "";
		   
	   }
       


	   echo $this->m_rektor->getdata($where);
   }
   
   public function add(){
		$nip = $this->input->post('nip');
   		$nama = $this->input->post('nama');
		$waktuakhirjabatan = $this->input->post('waktuakhirjabatan');
   	
		
		$data=array(
			'nip'=>$nip,
			'nama_rektor'=>$nama,
			'waktu_akhir_jabatan'=>$waktuakhirjabatan
		);
		
		
		$this->m_rektor->insert($data);
			
			
		//response
			$response=array(
				'pesan'=>'Data Berhasil disimpan',  	
				'status'=>'ok'
			);
			
			echo json_encode($response);
		
	}
   
   
   
   public function update(){
		$id = $this->input->post('id');	
		$nip = $this->input->post('nip');
   		$nama = $this->input->post('nama');
		$waktuakhirjabatan = $this->input->post('waktuakhirjabatan');
   	
		$where = array(

			'id_rektor' => $id
			
		);
		$data=array(
			'nip'=>$nip,
			'nama_rektor'=>$nama,
			'waktu_akhir_jabatan'=>$waktuakhirjabatan
		);
		
		
		$this->m_rektor->update($where,$data);
		
		
		
		$respon = array(

			'pesan' => "Data Berhasil Di Perbaharui",
			'status' => "success"
			
		);
			
		echo json_encode($respon);
		
       
    }
   
   	public function delete(){
   
		$id = $this->input->post('id');
		
	
		
		$data=array(
			'id_rektor'=>$id
		);
		
		
		$this->m_rektor->delete($data);
			
			
		//response
			$response=array(
				'pesan'=>'Data Berhasil dihapus',  	
				'status'=>'ok'
			);
			
			echo json_encode($response);
		
	}

	public function cekrektor(){
		
		
		
		$rektor=$this->m_rektor->get()->result_array();


		foreach ($rektor as $value) {

   		
			$date1=date_create($value['waktu_akhir_jabatan']);
			$date2=date_create(date("Y-m-d"));
			$diff  = date_diff($date1,$date2);
			$datediff=$diff->format("%R%d");

			//mengambil positif atau negatif
			$jumlahstring=strlen($datediff);
			$datediff=substr($datediff,-$jumlahstring,1); 

			if($datediff=='-'){

				$response=array(

					'status'=>'ok',
					'message'=>'Tidak Ada Masalah Dengan Data Rektor'


				);
				

				break;

			}else{

				$response=array(

					'status'=>'error',
					'message'=>'Waktu Jabatan Rektor Sudah Berakhir, Perbaharui Data Rektor'


				);
			}



		}



		
		
		
		echo json_encode($response);



	}
	   
	
    

}