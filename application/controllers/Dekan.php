<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dekan extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('m_dekan');
	}
	
	
	public function get(){
		$id = $this->input->post('id');	
		
		$where = array(

			'id_dekan' => $id
			
		);
		
		
		
		$data['data']=$this->m_dekan->get($where)->result_array();
		
		
		echo json_encode($data);
	}
	
	
	
	public function getdata(){
		
		
       if($this->uri->segment('3')){
			$where = array(

				'id_dekan' => $this->uri->segment('3')
					
			);
	   }else{
		   
		  $where = "";
		   
	   }
       


	   echo $this->m_dekan->getdata($where);
   }
   
   public function add(){
		$nip = $this->input->post('nip');
   		$nama = $this->input->post('nama');
		$waktuakhirjabatan = $this->input->post('waktuakhirjabatan');
   	
		
		$data=array(
			'nip'=>$nip,
			'nama_dekan'=>$nama,
			'waktu_akhir_jabatan'=>$waktuakhirjabatan
		);
		
		
		$this->m_dekan->insert($data);
			
			
		//response
			$response=array(
				'pesan'=>'Data Berhasil disimpan',  	
				'status'=>'ok'
			);
			
			echo json_encode($response);
		
	}
   
   
   
   public function update(){
		$id = $this->input->post('id');	
		$nip = $this->input->post('nip');
   		$nama = $this->input->post('nama');
		$waktuakhirjabatan = $this->input->post('waktuakhirjabatan');
   	
		$where = array(

			'id_dekan' => $id
			
		);
		$data=array(
			'nip'=>$nip,
			'nama_dekan'=>$nama,
			'waktu_akhir_jabatan'=>$waktuakhirjabatan
		);
		
		
		$this->m_dekan->update($where,$data);
		
		
		
		$respon = array(

			'pesan' => "Data Berhasil Di Perbaharui",
			'status' => "success"
			
		);
			
		echo json_encode($respon);
		
       
    }
   
	public function delete(){
	
		$id = $this->input->post('id');
		
	
		
		$data=array(
			'id_dekan'=>$id
		);
		
		
		$this->m_dekan->delete($data);
			
			
		//response
			$response=array(
				'pesan'=>'Data Berhasil dihapus',  	
				'status'=>'ok'
			);
			
			echo json_encode($response);
		
	}


   	public function cekdekan(){
		
		
		
	$dekan=$this->m_dekan->get()->result_array();


	foreach ($dekan as $value) {


		$date1=date_create($value['waktu_akhir_jabatan']);
		$date2=date_create(date("Y-m-d"));
		$diff  = date_diff($date1,$date2);
		$datediff=$diff->format("%R%d");

		//mengambil positif atau negatif
		$jumlahstring=strlen($datediff);
		$datediff=substr($datediff,-$jumlahstring,1); 

		if($datediff=='-'){

			$response=array(

				'status'=>'ok',
				'message'=>'Tidak Ada Masalah Dengan Data dekan'


			);
			

			break;

		}else{

			$response=array(

				'status'=>'error',
				'message'=>'Waktu Jabatan dekan Sudah Berakhir, Perbaharui Data Dekan'


			);
		}



	}






	echo json_encode($response);



	}
    

}