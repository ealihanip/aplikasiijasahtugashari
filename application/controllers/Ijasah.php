<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ijasah extends CI_Controller {
	public function __construct()
	{
		parent::__construct();

		$this->load->model('m_ijasah');
		$this->load->library('pdf');
	}
	
	public function index()
	{
	
		
	}

	public function getdata(){
		
		
		if($this->uri->segment('3')){
			 $where = array(
 
				 'id_dekan' => $this->uri->segment('3')
					 
			 );
		}else{
			
		   $where = "";
			
		}
		
 
 
		echo $this->m_ijasah->getdata($where);
	}

	public function cetak()
	{
		
		$no1=$this->input->post('no1');
		$no2=$this->input->post('no2');
		$nim=$this->input->post('nim');
		$nama=$this->input->post('nama');
		$tanggal_lahir=$this->input->post('tanggal_lahir');
		$tempat_lahir=$this->input->post('tempat_lahir');
		$jurusan=$this->input->post('jurusan');
		$tanggal_lulus=$this->input->post('tanggal_lulus');

		
		
		$where=array(
			'nim'=>$nim
		);

		//cari data 
		
		$data=$this->m_ijasah->getijasah($where)->num_rows();

		$nocetak=$this->m_ijasah->getnocetak()->num_rows();
		
		$nocetak=$nocetak+1;

		if(strlen($nocetak)==1){

			$nocetak='00'.($nocetak+1);

		}else if(strlen($nocetak)==2){

			$nocetak='0'.($nocetak+1);

		}else if(strlen($nocetak)==3){

			$nocetak=$nocetak+1;

		}

		if(strlen($data)==1){

			$data='00'.($data+1);

		}else if(strlen($data)==2){

			$data='0'.($data+1);

		}else if(strlen($data)==3){

			$data=$data+1;

		}

		$id=date('Y').$data.$nim;

		//mencari dekan saat lulus


		$dekan=$this->m_ijasah->getdekan()->result_array();


		foreach ($dekan as $value) {

   		
			$date1=date_create($value['waktu_akhir_jabatan']);
			$date2=date_create($tanggal_lulus);
			$diff  = date_diff($date1,$date2);
			$datediff=$diff->format("%R%d");

			//mengambil positif atau negatif
			$jumlahstring=strlen($datediff);
			$datediff=substr($datediff,-$jumlahstring,1); 

			if($datediff=='-'){

				$id_dekan_lulus=$value['id_dekan'];
				

				break;

			}else{

				
			}



		}

		//mencari dekan saat cetak


		$dekan=$this->m_ijasah->getdekan()->result_array();


		foreach ($dekan as $value) {

   		
			$date1=date_create($value['waktu_akhir_jabatan']);
			$date2=date_create(date("Y-m-d"));
			$diff  = date_diff($date1,$date2);
			$datediff=$diff->format("%R%d");

			//mengambil positif atau negatif
			$jumlahstring=strlen($datediff);
			$datediff=substr($datediff,-$jumlahstring,1); 

			if($datediff=='-'){

				$id_dekan_cetak=$value['id_dekan'];
				

				break;

			}else{

				
			}



		}

		//mencari rektor saat cetak


		$rektor=$this->m_ijasah->getrektor()->result_array();


		foreach ($rektor as $value) {

   		
			$date1=date_create($value['waktu_akhir_jabatan']);
			$date2=date_create(date("Y-m-d"));
			$diff  = date_diff($date1,$date2);
			$datediff=$diff->format("%R%d");

			//mengambil positif atau negatif
			$jumlahstring=strlen($datediff);
			$datediff=substr($datediff,-$jumlahstring,1); 

			if($datediff=='-'){

				$id_rektor=$value['id_rektor'];
				

				break;

			}else{

				
			}



		}

		

		
		$data=array(
			'id_cetak'=>$id,
			'seri_ijasah'=>$no1,
			'no_ijasah'=>$no2,
			'nim'=>$nim,
			'nama'=>$nama,
			'tempat_lahir'=>$tempat_lahir,
			'tanggal_lahir'=>$tanggal_lahir,
			'prodi'=>$jurusan,
			'tanggal_lulus'=>$tanggal_lulus,
			'dekan_lulus'=>$id_dekan_lulus,
			'dekan_cetak'=>$id_dekan_cetak,
			'rektor'=>$id_rektor,
			'tahun'=>date("Y"),
			'no_cetak'=>$nocetak
			
		);
		
		
		$this->m_ijasah->insert($data);
		

		
		
		

		$response=array(
			'pesan'=>'Berhsil cetak',
			'link'=>base_url().'ijasah/cetakijasah/'.$id,
			 	
			'status'=>'ok'
		);
			
		echo json_encode($response);
		
	}

	public function cetakulang(){

		$id=$this->input->post('idcetak');

		$response=array(
			'pesan'=>'Berhsil cetak',
			'link'=>base_url().'ijasah/cetakijasah/'.$id,
			 	
			'status'=>'ok'
		);

		echo json_encode($response);

	}
	
	public function cetakijasah()
	{	
		$id=$this->uri->segment('3');

		
	
		$where=array(
			'id_cetak'=>$id
		);
		$data=$this->m_ijasah->getcetak($where)->result_array();




		foreach ($data as $value){

			$ijasah['seri_ijasah']=$value['seri_ijasah'];
			$ijasah['no_ijasah']=$value['no_ijasah'];
			$ijasah['tahun']=$value['tahun'];
			$ijasah['nim']=$value['nim'];
            $ijasah['nama']=$value['nama'];
			$ijasah['tempat_lahir']=$value['tempat_lahir'];
			//ganti format tanggal
			$originalDate = $value['tanggal_lahir'];
			$newDate = date("F d, Y", strtotime($originalDate));


			$ijasah['tanggal_lahir']=$newDate;

			$originalDate = $value['tanggal_lulus'];
			$newDate = date("F d, Y", strtotime($originalDate));

			$ijasah['tanggal_lulus']=$newDate;
			$ijasah['dekan_lulus']=$value['dekan_lulus'];
			$ijasah['dekan_cetak']=$value['dekan_cetak'];
			$ijasah['rektor']=$value['rektor'];
			$ijasah['id_prodi']=$value['id_prodi'];
			$ijasah['nama_prodi']=$value['nama_prodi'];
			$ijasah['gelar']=$value['gelar'];
			$ijasah['id_rektor']=$value['id_rektor'];
			$ijasah['nama_rektor']=$value['nama_rektor'];
			$ijasah['nip']=$value['nip'];
			$ijasah['waktu_akhir_jabatan']=$value['waktu_akhir_jabatan'];
			$ijasah['nama_dekan_lulus']=$value['nama_dekan_lulus'];
			$ijasah['nip_dekan_lulus']=$value['nip_dekan_lulus'];
			$ijasah['nama_dekan_cetak']=$value['nama_dekan_cetak'];
			$ijasah['nip_dekan_cetak']=$value['nip_dekan_cetak'];
			$ijasah['tanggal_cetak']=date("F d, Y");
			$ijasah['no_cetak']=$value['no_cetak'];
									

		}

		

		
		$this->load->view('ijasah/cetakijasah',$ijasah);
		
	}


}