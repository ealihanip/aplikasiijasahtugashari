<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends CI_Controller {
	public function __construct() {
		
		parent::__construct();
		// $this->load->library('ion_auth');
		// $this->lang->load('auth');
		
		// if(!$this->ion_auth->logged_in()){
		// 	redirect('auth', 'refresh');
		// }
  	}
	
	public function index()
	{
		$this->load->view('partial/header');
		$this->load->view('partial/sidebar');
		
		$this->load->view('partial/footer');
	}


    public function rektor()
	{
		$this->load->view('partial/header');
		$this->load->view('partial/sidebar');
		$this->load->view('rektor/konten');
		$this->load->view('partial/footer');
	}

    public function dekan()
	{
		$this->load->view('partial/header');
		$this->load->view('partial/sidebar');
		$this->load->view('dekan/konten');
		$this->load->view('partial/footer');
	}

	public function ijasah()
	{
		$this->load->view('partial/header');
		$this->load->view('partial/sidebar');
		$this->load->view('ijasah/input_ijasah');
		$this->load->view('partial/footer');
	}

	public function prodi()
	{
		$this->load->view('partial/header');
		$this->load->view('partial/sidebar');
		$this->load->view('prodi/konten');
		$this->load->view('partial/footer');
	}

	public function historiijasah()
	{
		$this->load->view('partial/header');
		$this->load->view('partial/sidebar');
		$this->load->view('ijasah/histori_ijasah');
		$this->load->view('partial/footer');
	}

	public function datauser()
	{
		
		redirect('auth', 'refresh');
		
	}

}
