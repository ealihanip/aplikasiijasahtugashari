<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Prodi extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('m_prodi');
	}
	
	
	public function get(){
		$id = $this->input->post('id');	
		
		$where = array(

			'id_prodi' => $id
			
		);
		
		
		$data['data']=$this->m_prodi->get($where)->result_array();
		
		
		echo json_encode($data);
	}
	
	
	
	public function getdata(){
		
		
       if($this->uri->segment('3')){
			$where = array(

				'id_prodi' => $this->uri->segment('3')
					
			);
	   }else{
		   
		  $where = "";
		   
	   }
       


	   echo $this->m_prodi->getdata($where);
   }
   
   public function add(){
		$nama = $this->input->post('nama');
   		$gelar = $this->input->post('gelar');
		
   	
		
		$data=array(
			
			'nama_prodi'=>$nama,
			'gelar'=>$gelar
		);
		
		
		$this->m_prodi->insert($data);
			
			
		//response
			$response=array(
				'pesan'=>'Data Berhasil disimpan',  	
				'status'=>'ok'
			);
			
			echo json_encode($response);
		
	}
   
   
   
   public function update(){
		
		$id = $this->input->post('id');
		$nama = $this->input->post('nama');
   		$gelar = $this->input->post('gelar');
		
   	
		
		
   	
		$where = array(

			'id_prodi' => $id
			
		);
		$data=array(
			
			'nama_prodi'=>$nama,
			'gelar'=>$gelar
		);
		
		
		$this->m_prodi->update($where,$data);
		
		
		
		$respon = array(

			'pesan' => "Data Berhasil Di Perbaharui",
			'status' => "success"
			
		);
			
		echo json_encode($respon);
		
       
    }
   
   public function delete(){
   
   	$id = $this->input->post('id');
	
   
   	
   	$data=array(
   		'id_prodi'=>$id
   	);
   	
   	
   	$this->m_prodi->delete($data);
   		
   		
   	//response
   		$response=array(
			'pesan'=>'Data Berhasil dihapus',  	
   			'status'=>'ok'
   		);
   		
   		echo json_encode($response);
   	
   }
    

}