<div  id='forminputijasah'>


  <div class="row">

  <div class="col s12">
    <h1 class="center">Cetak Ijasah</h1>  
  </div>


  </div>


  <div class='row'>



  <div class="row">
    <div class='col s12'>

    <h5 class='black-text center'>Form Input Ijasah</h5>
    </div>
    <form class="col s12">
        <div class="row">
          <div class="col s2">
            <p class='black-text right'>NUMBER : Un.05/</p>
            
          </div>  
          <div class="col s1">
            <input id="no1" type="text" class="validate" placeholder='NUMBER'>
            
          </div>  

        </div>

        <div class="row">
          <div class="col s2">
            <p class='black-text right'>NUMBER : FST/S1/</p>
            
          </div>  
          <div class="col s1">
            <input id="no2" type="text" class="validate" placeholder='NUMBER'>
            
          </div>
          <div class="col s1">
            <p class='black-text'>/2018</p>
            
          </div>  

        </div>

        <div class="row">
          <div class="col s2">
            <p class='black-text right'>NIM</p>
            
          </div>  
          <div class="col s4">
            <input id="nim" type="text" class="validate" placeholder='NIM'>
            
          </div>


        </div>


        <div class="row">
          <div class="col s2">
            <p class='black-text right'>NAMA</p>
            
          </div>  
          <div class="col s4">
            <input id="nama" type="text" class="validate" placeholder='Nama'>
            
          </div>

        </div>

        <div class="row">
          <div class="col s2">
            <p class='black-text right'>Tempat Lahir</p>
            
          </div>  
          <div class="col s3">
            <input id="tempat_lahir" type="text" class="validate" placeholder='Tempat Lahir'>
            
          </div>

        </div>
        <div class="row">
          <div class="col s2">
            <p class='black-text right'>Tanggal Lahir</p>
            
          </div>  
          <div class="col s2">

            
            <input id="tanggal_lahir" type="text" class="datepicker" placeholder='Tanggals Lahir' >
            
          </div>

        </div>

        <div class="row">
          <div class="col s2">
            <p class='black-text right'>Jurusan</p>
            
          </div>  
          <div class="col s3">
          <select id='jurusan'>
            
          </select>
            
          </div>

        </div>

      

        <div class="row">
          <div class="col s2">
            <p class='black-text right'>Tanggal Lulus</p>
            
          </div>  
          <div class="col s2">
            <input id="tanggal_lulus" type="text" class="datepicker" placeholder='Tanggal Lulus'>
            
          </div>

        </div>

        <div class="row">
          <div class="col s2">
          
            
          </div>  
          <div class="col s2">
            <a class='btn blue' onclick='cetak();'>cetak</a>
            
          </div>

          


        </div>

        
      </form>
    
  </div>
  </div>

</div>



<script>
  init();

  function init(){
    $( document ).ready(function() {
      $('#tanggal_lahir').pickadate({
        selectMonths: true, // Creates a dropdown to control month
        selectYears: 15, // Creates a dropdown of 15 years to control year,
        format: 'yyyy-mm-dd',
        today: 'Today',
        clear: 'Clear',
        close: 'Ok',
        closeOnSelect: false, // Close upon selecting a date,
        max: new Date(2000,1,1),
              
      });


      $('#tanggal_lulus').pickadate({
        selectMonths: true, // Creates a dropdown to control month
        selectYears: 15, // Creates a dropdown of 15 years to control year,
        format: 'yyyy-mm-dd',
        today: 'Today',
        clear: 'Clear',
        close: 'Ok',
        closeOnSelect: false, // Close upon selecting a date,
        max: new Date(),
              
      });

  
      
     
           
            
            
       //select
      

      $('#forminputijasah').hide();
      getjurusan();
      cekRektor();
      cekDekan();
     
      

      
    });
    

  }


  function cetak(){
				 
		var data = new FormData();
		
		data.append('no1', $("#no1").val());
		data.append('no2', $("#no2").val());
		data.append('nim', $("#nim").val());
    data.append('nama', $("#nama").val());
    data.append('tempat_lahir', $("#tempat_lahir").val());
    data.append('tanggal_lahir', $("#tanggal_lahir").val());
    data.append('jurusan', $("#jurusan").val());
    data.append('tanggal_lulus', $("#tanggal_lulus").val());


		$(document).ready(function() {	
				
			$.ajax({
				url		: '<?php echo base_url('ijasah/cetak')?>',
				type	: 'post',
				processData: false,
				contentType: false,
				dataType: 'html',
				data	: data,
				beforeSend : function(){
					
				},
				success : function(data){
					
					var json = $.parseJSON(data);
					
					swal(json.pesan);

          window.open(json.link,"_blank");
				}
			});
		});	 
			 
	};

  
  function getjurusan() {
		
		
		
		var data = new FormData();
	
			$.ajax({
				url		: '<?php echo base_url('prodi/getdata')?>',
				type	: 'get',
				processData: false,
				contentType: false,
				dataType: 'html',
				data	: data,
				beforeSend : function(){
						
				},
				success : function(data){
						
					var json = $.parseJSON(data);
          
					
					for (x in json.data) {

            var select = document.getElementById("jurusan");
            var option = document.createElement("option");
            option.text = json.data[x].nama_prodi; ;
            option.value = json.data[x].id_prodi; ;

            select.add(option);
            
          }
         
					$('select').material_select();
        
				}
			});
		
	}


  function cekRektor(){

    var data = new FormData();
    
    $.ajax({
      url		: '<?php echo base_url('rektor/cekrektor')?>',
      type	: 'get',
      processData: false,
      contentType: false,
      dataType: 'html',
      data	: data,
      beforeSend : function(){
          
      },
      success : function(data){
          
        var json = $.parseJSON(data);
				
				


        if(json.status=='error'){
          swal(json.message); 
          $('#forminputijasah').html('<h1 class="center red-text">Error Pada Data</h1>');
          $('#forminputijasah').show();
        }else{
          $('#forminputijasah').show();
          
        }

        
        
        
        
      
      }
    });

  }


  function cekDekan(){

    var data = new FormData();

    $.ajax({
      url		: '<?php echo base_url('dekan/cekdekan')?>',
      type	: 'get',
      processData: false,
      contentType: false,
      dataType: 'html',
      data	: data,
      beforeSend : function(){
          
      },
      success : function(data){
          
        var json = $.parseJSON(data);
        
        


        if(json.status=='error'){
          
          swal(json.message); 
          $('#forminputijasah').html('<h1 class="center red-text">Error Pada Data</h1>');
          $('#forminputijasah').show();
        }else{

          $('#forminputijasah').show();
        }

        
        
        
        
      
      }
    });

  }



</script>