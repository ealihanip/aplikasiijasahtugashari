<html>

<head>



    <style>

        @page {
            
              

        }

        body{
            
            margin-left:2cm;
            margin-top:1cm;
            margin-right:2cm;
            background-color:white;
           

        }

        .papersize{

            
            width:257mm;
            height:190mm; 
           
            


        }

        .font-color{

            
            
           color:black;


        }

        .cetaktengah{


            width:237mm;
            margin:0 auto;

        }


        .col{

            
            float:left;
            height:auto;
        }

        .row{
            
            width:100%;


        }

        .s4{

            width:33.33%;
            

        }

        .s12{

            width:100%;
            

        }


        
        .font-size-10{

            font-size:10pt;
        }
        .font-size-12{

            font-size:12pt;
        }


        .font-size-14{

            font-size:14pt;
        }


        .bold{

            font-weight:bold;

        }

        .center{

            text-align:center;

        }


        .right{

            text-align:right;

        }

        .left{

            text-align:left;

        }


        .enter{
            width:100%;
            height:10pt;

        }





        .green{


            background-color:green;



        }

        .blue{


            background-color:blue;



        }

        .red{


            background-color:red;

        }

        .lambanggaruda{

            width:3.4cm;
            height:3cm;



        }

        .end{

            clear: both;
        }


        



    </style>

    <script type="text/javascript" src="<?php echo base_url();?>assets/bower_components/jquery/dist/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/bower_components/jspdf/dist/jspdf.min.js"></script>
	
	
	<script type="text/javascript" src="<?php echo base_url();?>assets/bower_components/jspdf-plugins/plugins/addhtml.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/bower_components/jspdf-plugins/plugins/split_text_to_size.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>/var/www/ijasah/assets/html2canvas/html2canvas.min.js"></script>
    
</head>



<body>
    
	
    <div id='cetak'class='cetak font-color papersize'>

        <div class='row '>
            <div class='col font-size-12 s4 left'>

                <div class='left'>NUMBER : Un.05/<?php echo $seri_ijasah;?></div>
                

            </div>


            <div class='col font-size-12 s4'>

                <div class='lambanggaruda'>

                </div>

            </div>

            <div class='col font-size-12 s4 right'>

                NUMBER FST/S1/<?php echo $no_ijasah;?>/<?php echo $tahun;?>

            </div>

            


        </div>

        <div class='row'>

            <div class='cetaktengah'>

            
                <div class='col s12 font-size-12 center bold'>
                    MINISTRY RELIGIOUS AFFAIR
                </div>
                <div class='col s12 font-size-12 center bold'>
                    STATE  ISLAMIC UNIVERSITY
                </div>
                <div class='col s12 font-size-12 center bold'>
                    SUNAN GUNUNG DJATI BANDUNG
                </div>

                <div class='row'>

                    <div class='col s12 font-size-12 enter'>

                    </div>
                    
                </div>

                <div class='row'>
                    <div class='col s12 font-size-12 center'>
                    herewith has declared that:
                    </div>
                </div>



                <div class='row'>

                    <div class='col s12 enter'>

                    </div>
                    
                </div>

                <div class='row'>
                    <div class='col s12  font-size-12 center bold'>
                    <?php echo $nama;?>
                    </div>
                </div>
                <div class='row'>
                    <div class='col s12 font-size-12 center'>
                    Student ID Number <?php echo $nim;?>
                    </div>
                </div>

                <div class='row'>

                    <div class='col s12 enter'>

                    </div>
                    
                </div>

                <div class='row'>

                    <div class='col s12 font-size-12'>
                        born in <?php echo $tempat_lahir;?>, <?php echo $tanggal_lahir;?>, has finished and fullfilled all the requements at Study 
                        Program of Physics,therefore the University has confered upon his the degree of:
                    </div>
                    
                </div>

                <div class='row'>

                    <div class='col s12 font-size-12 enter'>

                    </div>
                    
                </div>

                <div class='row'>

                    <div class='col s12 center font-size-14 bold'>
                        <?php echo $gelar;?>
                    </div>
                    
                </div>

                <div class='row'>

                    <div class='col s12 font-size-12 enter'>

                    </div>
                    
                </div>

                <div class='row'>

                    <div class='col s12 font-size-12'>
                        along with right and obligation atached in the degree,
                    </div>
                    <div class='col s12 font-size-12'>
                    Given in Bandung, <?php echo $tanggal_lulus;?>
                    </div>
                    
                </div>

                <div class='row'>

                    <div class='col s12 font-size-12 enter'>

                    </div>
                    
                </div>


                <div class='row'>

                    <div class='col s4 center font-size-12'>

                        <div class='col s12 center font-size-12'>
                            DEAN
                        </div>
                        <div class='col s12 center font-size-12'>
                            Faculty of Science and Technology
                        </div>

                        <div class='col s12 enter'>
                            
                        </div>

                        <div class='col s12 center font-size-12'>
                            Signed
                        </div>

                        <div class='col s12 enter'>
                            
                        </div>

                        <div class='col s12 center bold font-size-12'>
                            <?php echo $nama_dekan_lulus;?>
                        </div>

                        <div class='col s12 center font-size-12'>
                            NIP. <?php echo $nip_dekan_lulus;?>
                        </div>




                    



                    </div>
                    <div class='col s4 center font-size-12'>
                    &nbsp;
                    </div>

                    <div class='col s4 center font-size-12'>
                    <div class='col s12 center font-size-12'>
                            RECTOR
                        </div>

                        <div class='col s12 enter'>
                            
                        </div>

                        <div class='col s12 enter'>
                            
                        </div>

                        <div class='col s12 center font-size-12'>
                            Signed
                        </div>

                        <div class='col s12 enter'>
                            
                        </div>

                        <div class='col s12 center bold font-size-12'>
                            <?php echo $nama_rektor;?>
                        </div>

                        <div class='col s12 center font-size-12'>
                            NIP. <?php echo $nip;?>
                        </div>
                    </div>

                    <div class='col s12'>

                        <div class='col s12 center font-size-12'>
                            Date : <?php echo $tanggal_cetak;?>
                        </div>

                        <div class='col s12 center font-size-12'>
                            This is an authorized English Translation of the original dipoma
                        </div>

                        <div class='col s12 center font-size-12'>
                            issued by Islamic University Gunung Djati Bandung
                        </div>

                        <div class='col s12 enter'>
                            
                        </div>

                        <div class='col s12 center font-size-12'>
                            Faculty of Science and Technology
                        </div>

                        <div class='col s12 center font-size-12'>
                            Dean
                        </div>
                        
                        <div class='col s12 enter'>
                            
                        </div>

                        <div class='col s12 enter'>
                            
                        </div>

                        <div class='col s12 center bold font-size-12'>
                            <?php echo $nama_dekan_cetak;?>
                        </div>


                        <div class='col s12 center font-size-12'>
                            NIP. <?php echo $nip_dekan_cetak;?>
                        </div>   


                    </div>

                    <div class='col s12'>


                        <div class='col s12 left font-size-10'>
                            NL. <?php echo $no_cetak;?>
                        </div>   


                    </div>

                    
                    
                </div>


            </div>
        </div>    
    </div> 






    

    <script>

            
            html2canvas($("#cetak"), {
            onrendered: function(canvas) {         
                var imgData = canvas.toDataURL(
                    'image/png');              
                var doc = new jsPDF('l', 'mm','a4');
                doc.addImage(imgData, 'PNG', 20, 15);
                doc.save('sample-file.pdf');
            }
           });



            


    </script>
  
  

</body>


</html>