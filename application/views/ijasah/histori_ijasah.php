<div class="row">

	<div class="col s12">
		<h5 class="center">Histori Cetak Ijasah</h5>
	</div>

	
</div>


<div class='row'>

	

</div>

<div id='data' class='konten'>

	<div class="row">
		<div class="col s12">
			<a class="black-text waves-effect waves-light hoverable" onclick='menu("input");'><i class="green-text fas fa-plus-circle"></i> Add</a>
		</div>
		<div class="col s12">
			
			<table id="tabelhistoricetakijasah" class="ui celled table" style="width:100%">
				<thead>
					<tr>
						<th>ID Cetak</th>
						<th>Seri Ijasah</th>
						<th>No Ijasah</th>
						<th>NIM</th>
						<th>Nama</th>
						<th>Aksi</th>
					</tr>
				</thead>
				
			</table>


		</div>

	</div>

</div>

<div id='input' class='konten'>

	<div class="row">

		<div class="col s12">


			<div class="row">
				<div class="input-field col s3">
				<input id="tnip" type="text" class="validate">
				<label class="active" for="tnip">NIP</label>
				</div>
			</div>
		
			<div class="row">
				<div class="input-field col s6">
				<input id="tnama" type="text" class="validate">
				<label class="active" for="tnama">Nama dekan</label>
				</div>
			</div>
			

			<div class="row">
				<div class="input-field col s3">
				<input id="twaktuakhirjabatan" type="text" class="datepicker">
				<label class="active" for="twaktuakhirjabatan">Waktu Akhir Jabatan</label>
				</div>
			</div>
			
			<div class="row">
				<div class="input-field col s2">
					<a class="black-text waves-effect waves-light hoverable btn" onclick='adddata();'>Simpan</a>
				</div>
			</div>
		</div>
			
	</div>

</div>


<div id='edit' class='konten'>

	<div class="row">
	
		<div class="col s12">
			<input type="hidden" id="eid">




			<div class="row">
				<div class="input-field col s3">
				<input id="enip" type="text" class="validate">
				<label class="active" for="enip">NIP</label>
				</div>
			</div>
			<div class="row">
				<div class="input-field col s6">
				<input  id="enama" type="text" class="validate">
				<label class="active" for="enama">Nama dekan</label>
				</div>
			</div>
			
			<div class="row">
				<div class="input-field col s3">
				<input id="ewaktuakhirjabatan" type="text" class="datepicker">
				<label class="active" for="ewaktuakhirjabatan">Waktu Akhir Jabatan</label>
				</div>
			</div>
			
			<div class="row">
				<div class="input-field col s2">
					<a class="black-text waves-effect waves-light hoverable btn" onclick='updatedata();'>Simpan</a>
				</div>
			</div>
		</div>

	</div>

</div>

<script type="text/javascript">

	

	//call init
	init();
	menu('data');
	initddatatable();
	
	
	//init
	function init(){
		$( document ).ready(function() {
			$('.datepicker').pickadate({
				selectMonths: true, // Creates a dropdown to control month
				selectYears: 15, // Creates a dropdown of 15 years to control year,
				format: 'yyyy-mm-dd',
				today: 'Today',
				clear: 'Clear',
				close: 'Ok',
				closeOnSelect: false // Close upon selecting a date,
			});
		});
		

	}

	//init jquery data table 
	function initddatatable(){

		var save_method; //for save method string
		var table;

		$(document).ready(function() {
			//datatables
			table = $('#tabelhistoricetakijasah').DataTable({
				
				bLengthChange: false,
				info: false,
				pageLength: 10,
				oLanguage: {
					sSearch: "cari"
				},						
				processing: true, //Feature control the processing indicator.
				serverSide: true, //Feature control DataTables' server-side processing mode.
				order: [], //Initial no order.
				// Load data for the table's content from an Ajax source
				ajax: {
					url: '<?php echo base_url('ijasah/getdata'); ?>',
					type: "post"
				},
				//Set column definition initialisation properties.
				columns: [
					{"data": "id_cetak"},
					{"data": "seri_ijasah"},
					{"data": "no_ijasah"},
					{"data": "nim"},
                    {"data": "nama"},
					{"data": "aksi"}
				],
				columnDefs: [
					
				]
				
			});

		});



	}

	// hide all konten
	function hidekonten(){
		$('.konten').hide();
	}
	// function menu
	function menu(action){
			
		hidekonten();
		$('#'+action).show();

	}
	
	
	
	
function cetak(id){
		
	var data = new FormData();

	data.append('idcetak', id);
	


	$(document).ready(function() {	
			
		$.ajax({
			url		: '<?php echo base_url('ijasah/cetakulang')?>',
			type	: 'post',
			processData: false,
			contentType: false,
			dataType: 'html',
			data	: data,
			beforeSend : function(){
				
			},
			success : function(data){
				
				var json = $.parseJSON(data);
				
				swal(json.pesan);

				window.open(json.link,"_blank");
			}
		});
	});	 
			
};

	
	
</script>

