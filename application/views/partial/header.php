<!DOCTYPE html>
<html>
    <head>
		<!--font awesome-->
		<link href="<?php echo base_url();?>assets/bower_components/components-font-awesome/css/fontawesome-all.min.css" rel="stylesheet">
		<!--Import materialize.css-->
		<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/materialize/dist/css/materialize.min.css"  media="screen,projection"/>
		<!--ijasah css-->
		<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>assets/ijasah/ijasah.css"  media="screen,projection"/>
			<!--sweet alert css-->
		<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/sweetalert2/dist/sweetalert2.min.css"  media="screen,projection"/>
		
			
		<!--datatable semntic.css-->
		<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>assets/bower_components/datatables.net-se/css/dataTables.semanticui.min.css"  media="screen,projection"/>
		
		
		
		

		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

			
		<!--Import jQuery on top-->
		<script type="text/javascript" src="<?php echo base_url();?>assets/bower_components/jquery/dist/jquery.min.js"></script>
		
	</head>

	
	<body class="grey lighten-5">
		
		